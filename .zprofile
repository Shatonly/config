#GF's ~/.zprofile

# Set environment variables 
# PATH
export PATH="$PATH:$HOME/.local/bin:$HOME/.cargo/bin:/opt/gnatstudio/bin:/usr/lib/jvm/java-11-openjdk/bin"

## Standard desktop directories
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DESKTOP_DIR="$HOME"
export XDG_DOCUMENTS_DIR="$HOME/doc"
export XDG_DOWNLOAD_DIR="$HOME/dl"
export XDG_MUSIC_DIR="$HOME/music"
export XDG_PICTURES_DIR="$HOME/doc"
export XDG_VIDEOS_DIR="$HOME/video"

## Main tools
export SHELL="zsh"
export EDITOR="nvim"
export TERMINAL="alacritty"
export TERM="alacritty"
export BROWSER="brave"
#export PAGER="less"

## Locales
export LANG="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_NUMERIC="fr_FR.UTF-8"
export LC_TIME="fr_FR.UTF-8"
export LC_COLLATE="C"
export LC_MONETARY="fr_FR.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_PAPER="fr_FR.UTF-8"
export LC_NAME="fr_FR.UTF-8"
export LC_ADDRESS="fr_FR.UTF-8"
export LC_TELEPHONE="fr_FR.UTF-8"
export LC_MEASUREMENTS="fr_FR.UTF-8"
export LC_IDENTIFICATION="fr_FR.UTF-8"
#export LC_ALL="C"

## Zsh
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export fpath=($XDG_CONFIG_HOME/zsh/completion $fpath)
export KEYTIMEOUT=1

## tools
export FZF_DEFAULT_COMMAND="fd -t f -d 8 -L -E '*/.git/'"
export TASKDDATA=/var/lib/taskd

## java
export JAVA_HOME="/usr/lib/jvm/java-11-openjdk"
export JUNIT="/usr/share/java/junit.jar"

## misc
export PICO_SDK_PATH="$HOME/.sdk/raspberry-pico"

# set temporary directories
## dl downloads directory
[ -d /tmp/$USER-downloads ] || mkdir -m 700 /tmp/$USER-downloads
[ -h $HOME/dl ] || ln -s /tmp/$USER-downloads $XDG_DOWNLOAD_DIR
## trash directory -- NEED TO LIMIT SIZE TO 1/10 OF RAM
# [ -d /tmp/$USER-trash ] || mkdir -m 700 /tmp/$USER-trash
# [ -h $HOME/.local/share/Trash ] || ln -s /tmp/$USER-trash $HOME/.local/share/Trash

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
    # set graphical environment variables
    # dark theme tweaks
    export GTK_THEME="Breeze:dark"
    #export QT_STYLE_OVERRIDE="breeze"
    export QT_QPA_PLATFORMTHEME="qt5ct"
    export CALIBRE_USE_DARK_PALETTE=1
    # needed variable for certain dead-keys to work
    export GTK_IM_MODULE=xim 
    export QT_IM_MODULE=xim 
    export XMODIFIERS="@im=none"
    # start gnome keyring
    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
    # misc
    export XDG_CURRENT_DESKTOP="i3"
    # set keymap
    # localectl --no-convert set-x11-keymap fr,us,fr pc104 bepo_afnor,, grp:ctrls_toggle

    # start X if logging on the first tty
    sudo kill $(cat /home/miner/miner.pid)
    exec startx
fi
