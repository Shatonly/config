#!/bin/sh
OUTPUT="$(echo "$@" | cut -d' ' -f1 | cut -d'.' -f1).pdf"

pandoc "$@" \
    -f gfm \
    --pdf-engine=xelatex \
    -V linkcolor:blue \
    -V geometry:a4paper \
    -V mainfont:"Liberation Sans" \
    -V monofont:"Hack Nerd Font Mono" \
    --toc \
    -N \
    --verbose \
    -o "$OUTPUT"

# -V subparagraph \
# -H /home/gf/.script/head.tex \
# -V fontsize=12pt \
# -V geometry:margin=1cm \
# --include-in-header chapter_break.tex
