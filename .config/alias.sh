# sudo
# alias sudo='sudo -E '
alias su='sudo su'
alias sudo='sudo '
alias se='sudoedit '

# main cli tools
## listing
alias l='exa --icons --git'
alias ls='l -l --no-permissions'
alias lsd='ls --sort=age'
alias lss='ls --time-style full-iso'
alias ll='l -l --group'
alias la='l -l -all --group'
alias sl='ls --reverse'
alias sld='sl --sort=age'
alias al='la --reverse'
alias f='pcmanfm-qt & disown'
## files and directories
alias rm='rm -irv'
alias mr='trash -v'
alias ts='trash -v'
alias st='trash -v'
alias rmd='rmdir -v'
alias empty='trash-empty -i'
alias restore='trash-restore'
alias destroy='shred -vu'
alias mv='mv -v'
alias vm='mv -v'
alias cp='cp -irv'
alias cpn='cp -ruv'
alias pc='cp -iruv'
alias md='mkdir -p'
alias mkdir='mkdir -p'
alias n='nnn'
alias rs='rsync -Prluvh'
alias rsa='rsync -Pauvh'
alias mvdl='mv ~/dl/*'
alias cpdl='cp ~/dl/*'
## edit
alias vi='nvim'
alias e='$EDITOR'
alias et='${EDITOR}.test'
## open and search
alias c='cd'
alias d='z'
alias a='bat'
alias b='br'
alias fd="\fd -t f -d 10 -L -E '*/.git/' -E '*/.bare/'"
alias fdh="\fd -t f -d 6 -LH -E '*/.git/' -E '*/.bare/'"
alias fdi="\fd -t f -d 8 -LI -E '*/.git/' -E '*/.bare/'"
alias o="FZF_DEFAULT_COMMAND=\"\fd -t f -d 10 -L -E '*/.git/' -E '*/.bare/'\" open"
alias oh="FZF_DEFAULT_COMMAND=\"\fd -t f -d 6 -LH -E '*/.git/' -E '*/.bare/'\" open"
alias oi="FZF_DEFAULT_COMMAND=\"\fd -t f -d 8 -LI -E '*/.git/' -E '*/.bare/'\" open"
alias cc="cdf"
alias cn="term 'cdf;zsh' ~"
alias lu='rofi -combi-modi window,drun -font "hack 12" -show-icons -show combi'
## system management
alias pac='sudo pacman'
alias inst='sudo pacman -Sy --noconfirm'
alias up='sudo pacman -Syu --noconfirm'
alias mirrors='sudo reflector -c FR,ES,CH,IT,BE,GE -a 6 --sort rate --save /etc/pacman.d/mirrorlist'
alias mix='pulsemixer'
alias du='dust'
alias df='duf -hide special'
alias dfa='duf -all'
alias sys='sudo systemctl'
alias jo='journalctl -xe'
alias histedit='$EDITOR ~/.local/share/zhistory'
alias wi='nmcli device wifi'
## cli utilities
alias t='term'
alias ex='~/.local/bin/extract.sh'
alias xe='~/.local/bin/extract.sh'
alias rg='rg -S -C 2'
alias gr='rg -S -C 2'
alias re='exec zsh'
## others
alias py='python'
alias pi='ipython -i --ext=autoreload --ext=storemagic'
alias ju='term jupyter-lab .'
alias pre='preview'
alias wcp='wl-copy'
alias wpt='wl-paste'
# alias pdf='~/.local/bin/pandocToPDF.sh'
alias neo='neofetch'
alias year='cal -Y -w'
alias flash='kill $(pgrep flashfocus); flashfocus & disown'
alias quick='xset r rate 250 50'
# alias vmount='sudo mkdir -p /run/media/vm /run/media/sharedvm && sudo mount /run/media/vm && sudo mount /run/media/sharedvm'
### backup and snapshot
alias backup='~/.local/bin/backup.sh'
alias ventoyBackupDoc='zip -re /run/media/$USER/Ventoy/doc/doc.zip $HOME/doc'
alias ventoyBackup='zip -re /run/media/$USER/Ventoy/perso/perso.zip $HOME/perso \
                 && zip -re /run/media/$USER/Ventoy/project/project.zip $HOME/project \
                 && cp $HOME/.password-store /run/media/$USER/Ventoy/password-store'
alias inhib='systemd-inhibit sleep'

# parents directories
alias ...='../../'
alias ....='../../../'
alias .....='../../../../'
alias ......='../../../../../'

# removable devices
alias mtpmount='aft-mtp-mount ~/.mtp'
alias mtpumount='fusermount -u ~/.mtp'
alias usbumountall='udiskie-umount --all'
alias usbumount='udiskie-umount'
alias usbejectall='udiskie-umount --all --detach --eject'
alias usbeject='udiskie-umount --detach --eject'

# Git
alias status='git status'
alias fetchd='git fetch -v --dry-run'
alias fetch='git fetch -v'
alias gadd='git add'
alias gadda='git add --all'
alias commit='git commit -m'
alias commita='git commit -am'
alias acommit='git commit --amend'
alias acommita='git commit --amend -a'
alias push='git push'
alias pusha="git checkout main && git commit -am 'AUTO COMMIT from $(hostnamectl hostname)'; git push"
alias pushsub="git commit -am 'Updated submodule by $(hostnamectl hostname)'; git push"
alias pull='git pull'
alias checkout='git checkout'
alias main='git checkout main'
alias merge='git merge'
alias merget='git mergetool --tool=nvimdiff'
alias branch='git branch'
alias clean='git clean -idx'
alias unstage='git restore --staged'
alias untrack='git rm -r --cached'
alias gdiff='git diff'
alias glog='git log --oneline'
## Config files management
alias cfg='git --git-dir=$HOME/.bare/ --work-tree=$HOME'
alias cfgs='cfg status'
alias cfgcd='cd ~/.bare'
alias cfgbare='git init --bare $HOME/.bare \
    && git --git-dir=$HOME/.bare/ --work-tree=$HOME config --local status.showUntrackedFiles no'
alias cfgclone='git clone --bare git@gitlab.com:gfauredev/config.git $HOME/.bare \
    && git --git-dir=$HOME/.bare/ --work-tree=$HOME config --local status.showUntrackedFiles no \
    && git --git-dir=$HOME/.bare/ --work-tree=$HOME checkout'
alias cfgcommit='cfg commit -m'
alias cfgcommita='cfg commit -am'
alias cfgpull='cfg pull'
alias cfgpulla='cfg pull --all'
alias cfgpush='cfg push'
alias cfgpusha='cfgcommita "AUTO COMMIT from $(hostnamectl hostname)" ; cfgpush --all'
## Tasks management
alias s='task calendar && task'
alias tg='git -C $HOME/.task/'
alias tkp='git -C $HOME/.task/ commit -am "AUTO COMMIT from $(hostnamectl hostname)" ; git -C $HOME/.task/ push'

# bluetooth
alias bt='bluetoothctl'
alias btc='bluetoothctl connect'

# media controls
alias pp='playerctl play-pause'
alias next='playerctl next'
alias prev='playerctl previous'

# minig
alias mine='/home/miner/mine.sh'
alias showmine='tail -f /home/miner/nohup.out'
alias stopmine='sudo kill $(cat /home/miner/miner.pid)'

# misc
alias bios='sudo systemctl poweroff --firmware-setup'
alias hi='$TERM --class greet -e ~/.config/greeting.sh & disown'
alias winlinks='ln -sf ~/.wine/drive_c ~/.C.wine && ln -sf ~/.wine/drive_c/users/$USER ~/.$USER.wine && ln -sf /run/media/Windows/C ~/.C.win && ln -sf /run/media/Windows/C/Users ~/.$USER.win'
alias win="sudo efibootmgr -n $(efibootmgr | grep -i windows | cut -d'*' -f1 | tr -d 'Boot') && sudo reboot"
# alias lock='swaylock -f -i /usr/share/backgrounds/retroLandscape.jpg'
# alias dashrelink='sudo ln -svf /usr/bin/dash /usr/bin/sh'
# alias nvpersist='sudo nvidia-smi -pm 1'
# alias nvpower='sudo nvidia-smi -pl'

# graphical
# alias spotify='$BROWSER -app=https://open.spotify.com'
## games
# alias gfn='chromium -app=https://play.geforcenow.com & disown'
## graphical environment
alias swaytest='env __GL_GSYNC_ALLOWED=0 __GL_VRR_ALLOWED=0 WLR_DRM_NO_ATOMIC=1 QT_AUTO_SCREEN_SCALE_FACTOR=1 QT_QPA_PLATFORM=wayland QT_WAYLAND_DISABLE_WINDOWDECORATION=1 GDK_BACKEND=wayland XDG_CURRENT_DESKTOP=sway GBM_BACKEND=nvidia-drm __GLX_VENDOR_LIBRARY_NAME=nvidia MOZ_ENABLE_WAYLAND=1 WLR_NO_HARDWARE_CURSORS=1 sway --unsupported-gpu'
alias x='
    export GTK_THEME="Breeze:dark"
    export QT_QPA_PLATFORMTHEME="qt5ct"
    export CALIBRE_USE_DARK_PALETTE=1
    export GTK_IM_MODULE=xim 
    export QT_IM_MODULE=xim 
    export XMODIFIERS="@im=none"
    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
    export XDG_CURRENT_DESKTOP="Unity"
    exec startx'
