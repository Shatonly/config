# gf's config for sway
# Read `man 5 sway` for a complete reference

# variables
## modifier key (Mod1 for Alt)
set $mod Mod4
## direction keys
set $left c
set $down t
set $up s
set $right r
## terminal emulator
set $TERM alacritty

set $wallpaper $(\ls --sort time $HOME/.wallpaper/ | head -n 1)

# Output
## Wallpaper
output * bg $HOME/.wallpaper/$wallpaper fill
## gaps
gaps inner 5
# smart_gaps on
## Text
font pango:Hack Nerd Font Pro 10
title_align center
## Borders
default_border none
default_floating_border none
#titlebar_padding 4 4
# hide_edge_borders --i3 smart
hide_edge_borders --i3 smart_no_gaps
## Display configuration
output eDP-1 {
    resolution 1920x1080
}
output HDMI-A-1 {
    resolution 1920x1080 position 0 1080
}
output HDMI-A-2 {
    resolution 1920x1080 position 0 1080
}
# get outputs : swaymsg -t get_outputs

# Input
## Keyboards
input type:keyboard {
    xkb_layout "fr,us,fr"
    xkb_variant "bepo_afnor,,"
    xkb_options "grp:ctrls_toggle"
    repeat_delay 250
    repeat_rate 50
}
# clipboard survive to window closing
# exec wl-paste -t text --watch clipman store
## Touchpads
input type:touchpad {
    dwt enabled
    tap enabled
    natural_scroll disabled
    middle_emulation enabled
}
## Pointers
# input type:pointer {
# }
## Seat
seat * {
    hide_cursor when-typing enable
    xcursor_theme Vimix-cursors 24
}
exec gsettings set org.gnome.desktop.interface cursor-theme "Vimix-cursors"
# get inputs : swaymsg -t get_inputs

# bar
## waybar
bar {
    swaybar_command waybar
}

# idle
exec swayidle -w \
    timeout 300 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
    timeout 330 'swaylock -f -i $HOME/.wallpaper/$wallpaper' \
    timeout 600 'systemctl suspend' \
    before-sleep 'playerctl pause' \
    before-sleep 'swaylock -f -i $HOME/.wallpaper/$wallpaper'

# Key bindings
## System
### Start a terminal
default_orientation auto
bindsym $mod+Return exec $TERM
### start a floating terminal
bindsym $mod+Shift+Return exec $TERM --class menu
### kill focused window
bindsym $mod+q kill
### App launcher
#bindsym $mod+Space exec sirula
bindsym $mod+Space exec ulauncher
### Drag floating windows by holding down $mod and left mouse button.
### Resize them with right mouse button + $mod.
### Despite the name, also works for non-floating windows.
### Change normal to inverse to use left mouse button for resizing
# floating_modifier $mod normal
floating_modifier $mod
### reload the configuration file
bindsym $mod+Shift+q reload
### exit sway (logs you out of your Wayland session)
bindsym $mod+Control+q exec swaymsg exit
### suspend computer
bindsym $mod+rightsinglequotemark exec systemctl suspend
### power off computer
bindsym $mod+Shift+rightsinglequotemark exec poweroff
### reboot computer
bindsym $mod+Control+rightsinglequotemark exec reboot
### lock screen
bindsym $mod+Shift+l exec swaylock -f -i $HOME/.wallpaper/$wallpaper

## Moving
### Move focus around
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right
### Move the focused window with the same, but add Shift
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right
### Resizing
bindsym $mod+Control+$up resize grow height 10px
bindsym $mod+Control+$down resize shrink height 10px
bindsym $mod+Control+$right resize grow width 10px
bindsym $mod+Control+$left resize shrink width 10px

## Workspaces
# workspace_auto_back_and_forth yes
### Switch to workspace
bindsym $mod+a workspace 1:a
bindsym $mod+u workspace 2:u
bindsym $mod+i workspace 3:i
bindsym $mod+e workspace 4:e
bindsym $mod+comma workspace 5:comma
### Move focused container to workspace
bindsym $mod+Shift+a move container to workspace 1:a
bindsym $mod+Shift+u move container to workspace 2:u
bindsym $mod+Shift+i move container to workspace 3:i
bindsym $mod+Shift+e move container to workspace 4:e
bindsym $mod+Shift+comma move container to workspace 5:comma

## Layout
### Horizontal and vertical splits
bindsym $mod+n splith
bindsym $mod+v splitv
### Layout styles
bindsym $mod+Shift+n layout tabbed
bindsym $mod+Shift+v layout stacking
bindsym $mod+g layout toggle split
### Toggle the current focus between tiling and floating mode
bindsym $mod+f floating toggle
### Swap focus between the tiling area and the floating area
bindsym $mod+dead_circumflex focus mode_toggle
### Move focus to the parent container
bindsym $mod+z focus parent
### Move focus to the child container
bindsym $mod+j focus child
### Make the current focus fullscreen
bindsym $mod+w fullscreen global
### Focus output HDMI
bindsym $mod+Shift+w focus output HDMI-A-1

# Useful keybindings
## Open a file in a new term
bindsym $mod+o exec zsh -ic 'term oi menu'
### Open a directory in a new term
bindsym $mod+egrave exec zsh -ic 'ccn'

## Brightness control
bindsym $mod+ecircumflex exec brightnessctl set 5%-
bindsym $mod+Shift+ecircumflex exec brightnessctl set 2%-
bindsym $mod+Control+ecircumflex exec brightnessctl set 1%-
bindsym $mod+agrave exec brightnessctl set +5%
bindsym $mod+Shift+agrave exec brightnessctl set +2%
bindsym $mod+Control+agrave exec brightnessctl set +1%
### Media keys
bindsym XF86MonBrightnessDown exec brightnessctl set 5%-
bindsym XF86MonBrightnessUp exec brightnessctl set +5%
## Media controls
bindsym --locked $mod+p exec playerctl play-pause
bindsym --locked $mod+Shift+p exec playerctl next
bindsym --locked $mod+Control+p exec playerctl previous
bindsym --locked $mod+Control+Shift+p exec playerctl play-pause -p spotify
bindsym --locked $mod+x exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym --locked $mod+period exec pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym --locked $mod+Shift+period exec pactl set-sink-volume @DEFAULT_SINK@ +2%
bindsym --locked $mod+Control+period exec pactl set-sink-volume @DEFAULT_SINK@ +1%
bindsym --locked $mod+Shift+x exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym --locked $mod+Control+x exec pactl set-source-mute @DEFAULT_SOURCE@ toggle
### Media keys
bindsym --locked XF86AudioPause exec playerctl play-pause
bindsym --locked XF86AudioPlay exec playerctl play-pause
bindsym --locked XF86AudioNext exec playerctl next
bindsym --locked XF86AudioPrev exec playerctl previous
bindsym --locked XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym --locked XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym --locked XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym --locked XF86AudioMicMute exec pactl set-source-mute @DEFAULT_SOURCE@ toggle

# Applications keybindings
## Utilities
bindsym $mod+m exec zsh -ic "term pulsemixer menu"
bindsym $mod+l exec calibre
bindsym $mod+h exec zsh -ic "term htop menu"
bindsym $mod+d exec grim $HOME/.shot/$(date +'%d-%m-%Y_%H:%M:%S_capture.png')
bindsym $mod+Shift+d exec grim -g "$(slurp)" $HOME/.shot/$(date +'%d-%m-%Y_%H:%M:%S_capture.png')
bindsym $mod+k exec anki
# bindsym $mod+x exec marktext
## Browsers
bindsym $mod+b exec $BROWSER
bindsym $mod+Control+b exec chromium
bindsym $mod+Shift+b exec firefox
## misc 
bindsym $mod+Shift+m exec signal-desktop
bindsym $mod+Shift+o exec spotify || spotify-launcher || zsh -ic "$BROWSER -app=https://open.spotify.com"

# Window classes customization
for_window [app_id="menu"] floating enable, resize set width 888 px height 420 px
for_window [app_id="greet"] floating enable, resize set width 777 px height 777 px
for_window [app_id="task"] floating enable, resize set width 1111 px height 1037 px, move position 800 1

# Background startup programs
exec --no-startup-id mpris-proxy
exec --no-startup-id udiskie
exec --no-startup-id mako
exec_always --no-startup-id flashfocus
exec env RUST_BACKTRACE=1 RUST_LOG=swayr=debug swayrd > /tmp/swayrd.log 2>&1
# Starting workspace
workspace 3:i

# Foreground startup programs
exec $TERM --class greet -e $HOME/.config/greeting.sh
